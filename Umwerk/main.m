//
//  main.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
