//
//  ViewController.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//
#import "UserDetailleController.h"
#import "ViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "UserCell.h"
#import "User.h"
#import "API.h"


@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSMutableArray* userArray;
@property (strong,nonatomic) UIColor* backgroundColor;
@property (assign) NSUInteger page;
@property(assign)BOOL isIPad;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backgroundColor = [[AppDelegate instance]backgroundColor];
    self.view.backgroundColor      = self.backgroundColor;
    self.userArray = [[NSMutableArray alloc]init];
    self.page = 1;
    [self retrieveUsers:self.page];
    self.isIPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - tableviewdatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.page<100?self.userArray.count+1:self.userArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == self.userArray.count  ) {
        
        return self.isIPad?70:35;
    }
    return self.isIPad?100:70;
}

#pragma mark - tableviewdelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.userArray.count  ) {
        
        UITableViewCell *lastCell =  [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (!lastCell) {
            lastCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        lastCell.textLabel.text = @"Loding more ...";
        lastCell.textLabel.textColor = self.backgroundColor;
        self.page++;
        [self retrieveUsers:self.page];
        return  lastCell;
        
    }else{
        
        UserCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
        if (!cell) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"UserCell" owner:nil options:nil][0];
        }
        User * user = [self.userArray objectAtIndex:indexPath.row];
        cell.user = user;
        return cell;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    User * u = [self.userArray objectAtIndex:indexPath.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserDetailleController* userDetail = (UserDetailleController* )[storyboard instantiateViewControllerWithIdentifier:@"UserDetailleController"];
    userDetail.user = u;
    [self.navigationController pushViewController:userDetail animated:YES];
}

#pragma mark - API CALL

-(void)retrieveUsers:(NSUInteger)page{
    if ([self connected]) {
        [self.indicator startAnimating];
        self.indicator.hidden=NO;
        if (self.page<100) {
            [API getItems:page completion:^(NSDictionary *dict, NSString *errorMessage) {
                
                if (errorMessage == nil) {
                    
                    NSArray * users = [dict objectForKey:@"items"];
                    for(NSDictionary* user in users){
                        [API getUser:[user objectForKey:@"url"] completion:^(NSDictionary *userDetailed, NSString *errorMessage) {
                            
                            if (errorMessage == nil) {
                                User * u = [[User alloc]initWithDict:userDetailed];
                                [self.userArray addObject:u];
                                if (self.userArray.count == self.page * 10) {
                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                        // to get the list of user ordred just uncomment the next line
                                        //[self.userArray sortUsingFunction:sortTagByUserName context:nil];
                                        self.indicator.hidden=YES;
                                        [self.indicator stopAnimating];
                                        [self.tableView reloadData];
                                    }];
                                }
                            }else{
                                [self AlertError:errorMessage];
                            }
                        }];
                    }
                }else{
                    [self AlertError:errorMessage];
                }
            }];
        }else{
            [self AlertError:@"Only the first 1000 search results are available!"];
        }
    }else{
        [self AlertError:@"Please check your internet connection"];
    }
}

#pragma mark- Sort

NSComparisonResult sortTagByUserName(User *tag1, User *tag2, void *ignore)
{
    return [tag1.username caseInsensitiveCompare:tag2.username];
}

- (IBAction)sortList:(id)sender {
    [self.userArray sortUsingFunction:sortTagByUserName context:nil];
    [self.tableView reloadData];
}

#pragma mark- Alert

-(void)AlertError:(NSString*)errorMessage{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error!!" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self retrieveUsers:self.page];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alert addAction:okAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.indicator.hidden=YES;
        [self.indicator stopAnimating];
        [self.tableView reloadData];
    }];
}

#pragma mark- Test Internet Connection

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}



@end
