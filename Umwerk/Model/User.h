//
//  User.h
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* html_url;
@property (nonatomic, strong) NSString* company;
@property (nonatomic, strong) NSString* blog;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* created;
@property (nonatomic, strong) NSString* updated;

@property (assign) int userId;
@property (assign) int  followers;
@property (assign) int following;

-(id)initWithDict:(NSDictionary*)dict;

@end
