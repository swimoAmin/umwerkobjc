//
//  User.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import "User.h"

@implementation User
- (id) initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        self.username  = [[dict  objectForKey:  @"login"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"login"];
        self.image     = [[dict  objectForKey:  @"avatar_url"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"avatar_url"];
        self.userId    = [[dict  objectForKey:  @"id"]intValue];
        self.name      = [[dict  objectForKey:  @"name"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"name"];
        self.html_url  = [[dict  objectForKey:  @"html_url"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"html_url"];
        self.company   = [[dict  objectForKey:  @"company"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"company"];
        self.blog      = [[dict  objectForKey:  @"blog"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"blog"];
        self.location  = [[dict  objectForKey:  @"location"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"location"];
        self.email     = [[dict  objectForKey:  @"email"]isKindOfClass:[NSNull class]]? @"N/A":[dict  objectForKey:@"email"];
        self.created   = [dict  objectForKey:   @"created_at"];
        self.updated   = [dict  objectForKey:   @"updated_at"];
        self.followers = [[dict objectForKey:   @"followers"]intValue];
        self.following = [[dict objectForKey:   @"following"]intValue];
        
    }
    return self;
}


@end
