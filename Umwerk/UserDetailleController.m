//
//  UserDetailleController.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import "UserDetailleController.h"
#import "IconDownloader.h"
#import "AppDelegate.h"

@interface UserDetailleController ()

@property (strong, nonatomic) IconDownloader *iconDownloader;
@property (strong,nonatomic) UIColor* backgroundColor;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *followerLabel;
@property (weak, nonatomic) IBOutlet UIButton *githubButton;
@property (weak, nonatomic) IBOutlet UIButton *blogButton;

#pragma mark- Constraint attr
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailing;
@end

@implementation UserDetailleController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backgroundColor = [[AppDelegate instance]backgroundColor];
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:NULL action:NULL];
    self.navigationController.navigationBar.tintColor = self.backgroundColor;
    self.navigationItem.title = self.user.username;
    self.view.backgroundColor = self.backgroundColor;
    [self fillUserDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Populate User Details

-(void)fillUserDetails{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.leading.priority  = 750;
        self.trailing.priority = 250;
        self.userImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    self.iconDownloader = [[IconDownloader alloc] init];
    [self addBorder:self.githubButton];
    [self addBorder:self.blogButton];
    self.userNameLabel.text = self.user.username;
    self.addressLabel.text = self.user.location;
    self.emailLabel.text = self.user.email;
    self.followerLabel.text = [NSString stringWithFormat:@"%d Followers",self.user.followers];
    self.iconDownloader.iconUrl = self.user.image;
    self.iconDownloader.fileName = [NSString stringWithFormat:@"user_%d.png", self.user.userId ];
    __weak UserDetailleController* weakSelf = self;
    [self.iconDownloader setCompletionHandler:^(UIImage *image, BOOL shouldRefresh){
        if (image == nil) {
            weakSelf.userImage.image = [UIImage imageNamed:@"avatar_big"];
        }else{
            weakSelf.userImage.image = image;
        }
    }];
    [self.iconDownloader startDownload];
    self.blogButton.hidden = ![self.user.blog containsString:@"www"] && ![self.user.blog containsString:@"http://"] ;
}

#pragma mark- Navigate to browser

- (IBAction)openBlog:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString:self.user.blog] options:@{} completionHandler:nil];
}
- (IBAction)openGithubProfile:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString:self.user.html_url] options:@{} completionHandler:nil];
}

#pragma mark- Add border

-(void)addBorder:(UIButton*)button{
    button.layer.cornerRadius = 4;
    button.layer.borderWidth  = 2;
    button.layer.borderColor  = [UIColor whiteColor].CGColor;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
