//
//  UserDetailleController.h
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UserDetailleController : UIViewController

@property(strong,nonatomic) User* user;

@end
