//
//  API.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import "API.h"
#define USER_URL @"https://api.github.com/search/users?q=language%%3Ajava&page=%lu&per_page=10"

@implementation API

+ (NSURLSessionTask *)getItems:(NSUInteger)attr completion:(void (^)(NSDictionary *dict, NSString *errorMessage))completion{
    
    NSURL *URL = [NSURL URLWithString: [NSString stringWithFormat:USER_URL,(unsigned long)attr]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            if (completion != nil) {
                completion(nil, error.localizedDescription);
            }
            return;
        }
        NSError *jsonError = nil;
        if ([response isKindOfClass:NSHTTPURLResponse.class])
        {
            NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
            if (statusCode != 200) {
                if (completion != nil) {
                    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                    completion(nil, [dict objectForKey:@"message"]);
                }
                return;
            }
        }
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        if (jsonError != nil)
        {
            completion(nil, jsonError.localizedDescription);
            return;
        }
        completion(dict, nil);
        
    }];
    [dataTask resume];
    return dataTask;
}
+ (NSURLSessionTask *)getUser:(NSString*)url completion:(void (^)(NSDictionary *dict, NSString *errorMessage))completion{
    
    NSURL *URL = [NSURL URLWithString: url];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            if (completion != nil) {
                completion(nil, error.localizedDescription);
            }
            return;
        }
        
        NSError *jsonError = nil;
        if ([response isKindOfClass:NSHTTPURLResponse.class])
        {
            NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
            if (statusCode != 200) {
                if (completion != nil) {
                    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                    completion(nil, [dict objectForKey:@"message"]);
                }
                return;
            }
        }
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        if (jsonError != nil)
        {
            completion(nil, jsonError.localizedDescription);
            return;
        }
        completion(dict, nil);
    }];
    [dataTask resume];
    return dataTask;
}



@end
