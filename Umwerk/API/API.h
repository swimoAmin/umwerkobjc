//
//  API.h
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface API : NSObject
+ (NSURLSessionTask *)getItems:(NSUInteger)attr completion:(void (^)(NSDictionary *dict, NSString *errorMessage))completion;
+ (NSURLSessionTask *)getUser:(NSString*)url completion:(void (^)(NSDictionary *dict, NSString *errorMessage))completion;
@end
