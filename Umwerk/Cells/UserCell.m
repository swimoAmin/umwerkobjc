//
//  UserCell.m
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import "UserCell.h"
#import "IconDownloader.h"
@interface UserCell()

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registerDateLabel;
@property (strong, nonatomic) IconDownloader *iconDownloader;

@end

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconDownloader = [[IconDownloader alloc] init];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mmark- Set User Detail

-(void)setUser:(User *)user{
    _user = user;
    self.usernameLabel.text = user.username;
    self.avatar.layer.cornerRadius = 27;
    self.avatar.clipsToBounds = YES;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    [formatter setLocale:[NSLocale currentLocale]];
    NSDate * creatdDate = [formatter dateFromString:user.created ];
    formatter.dateFormat = @"dd-MM-yyyy";
    NSString * registerDate= [formatter stringFromDate:creatdDate];
    self.registerDateLabel.text = [NSString stringWithFormat:@"Member since: %@",registerDate];
    self.iconDownloader.iconUrl = user.image;
    self.iconDownloader.fileName = [NSString stringWithFormat:@"user_%d.png", user.userId ];
    __weak UserCell* weakSelf = self;
    [self.iconDownloader setCompletionHandler:^(UIImage *image, BOOL shouldRefresh){
        if (image == nil) {
            weakSelf.avatar.image = [UIImage imageNamed:@"avatar"];
        }else{
            weakSelf.avatar.image = image;
        }
    }];
    [self.iconDownloader startDownload];
    
    
    
    
    
}

@end
