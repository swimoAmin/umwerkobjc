//
//  AppDelegate.h
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic) UIColor* backgroundColor;
+ (AppDelegate *)instance;

@end

