//
//  TodayViewController.m
//  UmwerToday
//
//  Created by mohamed souiden on 2017. 01. 14..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "UserCell.h"
#import "API.h"
#import "User.h"

@interface TodayViewController () <NCWidgetProviding,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSMutableArray* userArray;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self retrieveUsers];
    self.extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayModeExpanded;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    
    completionHandler(NCUpdateResultNewData);
}
- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize {
    if (activeDisplayMode == NCWidgetDisplayModeExpanded) {
        self.preferredContentSize = CGSizeMake(0.0, 210.0);
    } else if (activeDisplayMode == NCWidgetDisplayModeCompact) {
        self.preferredContentSize = maxSize;
    }
}

#pragma mark - tableviewdatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.userArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}

#pragma mark - tableviewdelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UserCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"UserCell" owner:nil options:nil][0];
    }
    User * user = [self.userArray objectAtIndex:indexPath.row];
    cell.user = user;
    return cell;
}

-(void)retrieveUsers{
    
    [self.indicator startAnimating];
    self.indicator.hidden=NO;
    
    NSInteger r1 = (0 + arc4random_uniform(3 - 0 + 1));
    NSInteger r2 = (4 + arc4random_uniform(7 - 4 + 1));
    NSInteger r3 = (8 + arc4random_uniform(9 - 8 + 1));
    
    [API getItems:r2 completion:^(NSDictionary *dict, NSString *errorMessage) {
        
        if (errorMessage == nil) {
            NSArray * users = [dict objectForKey:@"items"];
            self.userArray = [[NSMutableArray alloc]init];
            for(int i = 0;i<users.count;i++){
                if (i == r1 || i == r2 || i==r3) {
                    NSDictionary* user = [users objectAtIndex:i];
                    [API getUser:[user objectForKey:@"url"] completion:^(NSDictionary *userDetailed, NSString *errorMessage) {
                        
                        if (errorMessage == nil) {
                            
                            User * u = [[User alloc]initWithDict:userDetailed];
                            [self.userArray addObject:u];
                            if (self.userArray.count == 3) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    self.indicator.hidden=YES;
                                    [self.indicator stopAnimating];
                                    [self.userArray sortUsingFunction:sortTagByUserName context:nil];
                                    [self.tableView reloadData];
                                }];
                            }
                        }else{
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                self.indicator.hidden=YES;
                                [self.indicator stopAnimating];
                                [self retrieveUsers];
                            }];
                        }
                    }];
                }
                
            }
        }
    }];
}

#pragma mark- Sort

NSComparisonResult sortTagByUserName(User *tag1, User *tag2, void *ignore)
{
    return [tag1.username caseInsensitiveCompare:tag2.username];
}
@end
